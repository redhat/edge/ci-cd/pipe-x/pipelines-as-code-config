# pipelines-as-code-config

Pipeline variables for [pipelines-as-code](https://gitlab.com/redhat/edge/ci-cd/pipe-x/pipelines-as-code-config)
 instance in the Pipe-X namespace

## Contibuting

### pre-commit integration

With **pre-commit** you can test your code for small issues and run the appropriate
linters locally.

> :pushpin: To make use of this feature you will need to [install Pre-commit](#installing-pre-commit)
on your local machine.

To enable this, simply run `pre-commit install` from the project directory at any
stage after cloning and before committing. Now, every `git commit` run will be
followed by the hooks defined in [.pre-commit-config.yaml](.pre-commit-config.yaml).
Unless all tests have passed, the commit will be aborted.

#### Running hooks

The configured hooks will only run against the files that have been modified or added.
To allow testing on all files in the repository instead, you can use the following
command:

```shell
pre-commit run --all-files
```

and to run individual hooks:

```shell
pre-commit run <hook_id>
```

> :pushpin: Some hooks have local dependencies (e.g. markdownlint requires RubyGems)

If you wish to perform pre-commit testing but want to skip any specific test/hook
use SKIP on commit.
The SKIP environment variable is a comma separated list of hook ids as defined
in [.pre-commit-config.yaml](.pre-commit-config.yaml)

```shell
SKIP=<hook_id>, <hook_id> git commit -m "foo"
```

If you decide not to use pre-commit after enablement, `pre-commit uninstall` will
restore your hooks to the state prior to installation.
Alternatively, you can run your commit with `--no-verify`.

#### Installing Pre-commit

To install the pre-commit package manager, run the respective command for your preferred
package manager:

```shell
pip install pre-commit
```

```shell
brew install pre-commit
```

```shell
conda install -c conda-forge pre-commit
```
